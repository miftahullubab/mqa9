const students = [
    {
      id: 1,
      name: 'Indra Diningrat',
      age: 24,
    },
    {
      id: 2,
      name: 'Bedjo Doe',
      age: 26,
    },
    {
      id: 3,
      name: 'Ahmad Dalan',
      age: 19,
    },
 ];
 export default students;